/*
    Intermodal's router
    Copyright (C) 2022  Cláudio Pereira

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::fs;
use std::rc::Rc;

use geo_types::point;
use itertools::Itertools;
use sqlx::PgPool;

use crate::cache::storage::WorldCache;
use crate::optimization::cluster_stops;
use crate::pathfinder::{NodeId, NodeRouteSuccessors, Nodes, SubrouteSuccessor, Subroutes, World};
use crate::{get_distance_between_points, sql};

pub async fn load_world() -> World {
    if let Ok(string) = fs::read_to_string("./cache/world.json") {
        let graph: storage::WorldCache = serde_json::from_str(&string).unwrap();
        graph.into()
    } else {
        let pool = PgPool::connect(
            "postgres://user:password@server/db",
        )
        .await
        .expect("Unable to connect to the database");

        let stops = sql::fetch_stops(&pool)
            .await
            .unwrap()
            .into_iter()
            .map(|stop| (stop.id, Rc::new(stop)))
            .collect::<Nodes>();
        let clusters = cluster_stops(stops.values().cloned().collect_vec());

        let mut node_succs = NodeRouteSuccessors::new();

        let subroutes = sql::fetch_subroutes(&pool)
            .await
            .unwrap()
            .into_iter()
            .map(|(id, subroute)| (id, Rc::new(subroute)))
            .collect::<Subroutes>();

        let mut distmap: HashMap<(NodeId, NodeId), (f64, f64)> = HashMap::new();

        for subroute_stops in sql::fetch_all_subroute_stops(&pool).await.unwrap() {
            for (idx, (from_stop, to_stop)) in subroute_stops
                .stops
                .iter()
                .zip(subroute_stops.stops.iter().skip(1))
                .enumerate()
            {
                if !distmap.contains_key(&(*from_stop, *to_stop)) {
                    let stop_a = stops.get(from_stop).unwrap();
                    let stop_b = stops.get(to_stop).unwrap();
                    let a = point! {x: stop_a.lon.unwrap(), y:stop_a.lat.unwrap()};
                    let b = point! {x: stop_b.lon.unwrap(), y:stop_b.lat.unwrap()};
                    let dist = get_distance_between_points(&a, &b).await.unwrap();
                    println!("{:?} to {:?} is {:?}", a, b, dist);
                    distmap.insert((*from_stop, *to_stop), dist);
                }

                let succ = SubrouteSuccessor {
                    subroute: subroutes.get(&subroute_stops.subroute).unwrap().clone(),
                    stop: stops
                        .get(to_stop)
                        .expect(&format!("Stop {} does not exist", to_stop))
                        .id,
                    idx,
                };
                match node_succs.entry(*from_stop) {
                    Entry::Occupied(mut entry) => {
                        entry.get_mut().push(succ);
                    }
                    Entry::Vacant(entry) => {
                        entry.insert(vec![succ]);
                    }
                }
            }
        }

        let world = World {
            nodes: stops,
            clusters,
            subroutes,
            node_succs,
            dists: distmap,
        };
        let cache = WorldCache::from(world.clone());
        fs::write(
            "./cache/world.json",
            serde_json::to_string_pretty(&cache).unwrap(),
        )
        .unwrap();
        world
    }
}

pub(crate) mod storage {
    use crate::pathfinder::NodeId;
    use crate::{models, pathfinder};
    use crate::{RouteId, Stop, SubrouteId, World};
    use itertools::Itertools;
    use serde::{Deserialize, Serialize};
    use std::collections::{BTreeSet, HashMap};
    use std::rc::Rc;

    pub type Nodes = HashMap<NodeId, Rc<Stop>>;

    #[derive(Serialize, Deserialize)]
    pub struct SubrouteSuccessor {
        pub subroute: SubrouteId,
        pub stop: NodeId,
        pub idx: usize,
    }

    pub type RouteStopSuccessors = HashMap<NodeId, Vec<SubrouteSuccessor>>;

    #[derive(Serialize, Deserialize)]
    pub struct WorldCache {
        pub nodes: Nodes,
        pub clusters: HashMap<NodeId, BTreeSet<NodeId>>,
        pub routes: Vec<Route>,
        pub node_succs: RouteStopSuccessors,
        pub dists: Vec<((NodeId, NodeId), (f64, f64))>,
    }

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Route {
        pub(crate) id: RouteId,
        pub(crate) operator: i32,
        pub(crate) subroutes: Vec<Subroute>,
        pub(crate) code: String,
        pub(crate) name: String,
    }

    #[derive(Serialize, Deserialize, Clone, Debug)]
    pub struct Subroute {
        pub(crate) id: SubrouteId,
        pub(crate) flag: String,
    }

    impl From<models::Subroute> for Subroute {
        fn from(subroute: models::Subroute) -> Self {
            Subroute {
                id: subroute.id,
                flag: subroute.flag,
            }
        }
    }

    impl From<World> for WorldCache {
        fn from(graph: World) -> Self {
            let mut routes = HashMap::<RouteId, Route>::new();
            graph.subroutes.values().for_each(|subroute| {
                let new_subroute = Subroute {
                    id: subroute.id,
                    flag: subroute.flag.clone(),
                };
                routes
                    .entry(subroute.route.id)
                    .and_modify(|route| {
                        route.subroutes.push(new_subroute.clone());
                    })
                    .or_insert(Route {
                        id: subroute.route.id,
                        operator: subroute.route.operator,
                        subroutes: vec![new_subroute],
                        code: subroute.route.code.clone(),
                        name: subroute.route.name.clone(),
                    });
            });

            let node_succs = graph
                .node_succs
                .iter()
                .map(|(stop, successors)| {
                    (
                        *stop,
                        successors
                            .iter()
                            .map(|successor| SubrouteSuccessor {
                                subroute: successor.subroute.id,
                                stop: successor.stop,
                                idx: successor.idx,
                            })
                            .collect_vec(),
                    )
                })
                .collect::<HashMap<NodeId, Vec<SubrouteSuccessor>>>();

            let dists = graph.dists.into_iter().map(|(k, v)| (k, v)).collect();
            Self {
                nodes: graph.nodes,
                clusters: graph.clusters,
                routes: routes.into_values().collect_vec(),
                node_succs,
                dists,
            }
        }
    }

    impl From<WorldCache> for World {
        fn from(world: WorldCache) -> Self {
            let subroutes = world
                .routes
                .into_iter()
                .flat_map(|route| {
                    let new_route = Rc::new(models::Route {
                        id: route.id,
                        operator: route.operator,
                        code: route.code.clone(),
                        name: route.name,
                    });

                    route
                        .subroutes
                        .iter()
                        .map(|subroute| {
                            (
                                subroute.id,
                                Rc::new(models::Subroute {
                                    id: subroute.id,
                                    flag: subroute.flag.clone(),
                                    route: new_route.clone(),
                                }),
                            )
                        })
                        .collect_vec()
                })
                .collect::<HashMap<SubrouteId, Rc<models::Subroute>>>();

            let node_succs = world
                .node_succs
                .iter()
                .map(|(stop, successors)| {
                    (
                        *stop,
                        successors
                            .iter()
                            .map(|successor| pathfinder::SubrouteSuccessor {
                                subroute: subroutes.get(&successor.subroute).unwrap().clone(),
                                stop: successor.stop,
                                idx: successor.idx,
                            })
                            .collect_vec(),
                    )
                })
                .collect::<HashMap<NodeId, Vec<pathfinder::SubrouteSuccessor>>>();

            let dists = world
                .dists
                .into_iter()
                .map(|(k, v)| (k, v))
                .collect::<HashMap<(NodeId, NodeId), (f64, f64)>>();

            Self {
                nodes: world.nodes,
                clusters: world.clusters,
                subroutes,
                node_succs,
                dists,
            }
        }
    }
}
