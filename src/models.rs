/*
    Intermodal's router
    Copyright (C) 2022  Cláudio Pereira

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::fmt::{Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::rc::Rc;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Default)]
pub struct Stop {
    pub id: i32,
    pub source: String,
    pub name: Option<String>,
    pub official_name: Option<String>,
    pub osm_name: Option<String>,
    pub short_name: Option<String>,
    pub locality: Option<String>,
    pub street: Option<String>,
    pub door: Option<String>,
    pub parish: Option<i32>,
    pub lat: Option<f64>,
    pub lon: Option<f64>,
    pub external_id: Option<String>,
    pub succeeded_by: Option<i32>,
    pub notes: Option<String>,
    pub has_crossing: Option<bool>,
    pub has_accessibility: Option<bool>,
    pub has_abusive_parking: Option<bool>,
    pub has_outdated_info: Option<bool>,
    pub is_damaged: Option<bool>,
    pub is_vandalized: Option<bool>,
    pub has_flag: Option<bool>,
    pub has_schedules: Option<bool>,
    pub has_sidewalk: Option<bool>,
    pub has_shelter: Option<bool>,
    pub has_bench: Option<bool>,
    pub has_trash_can: Option<bool>,
    pub is_illuminated: Option<bool>,
    pub has_illuminated_path: Option<bool>,
    pub has_visibility_from_within: Option<bool>,
    pub has_visibility_from_area: Option<bool>,
    pub is_visible_from_outside: Option<bool>,
    pub updater: i32,
    pub update_date: String,

    pub tags: Vec<String>,
}

impl Debug for Stop {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.id))
    }
}

impl PartialEq<Self> for Stop {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for Stop {}

impl Hash for Stop {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state)
    }
}

impl Display for Stop {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(name) = &self.name {
            f.write_fmt(format_args!(
                "{} ({:.5}, {:.5})",
                name,
                self.lat.unwrap(),
                self.lon.unwrap()
            ))
        } else if let Some(name) = &self.osm_name {
            f.write_fmt(format_args!(
                "{} ({:.5}, {:.5})",
                name,
                self.lat.unwrap(),
                self.lon.unwrap()
            ))
        } else if let Some(name) = &self.official_name {
            f.write_fmt(format_args!(
                "{} ({:.5}, {:.5})",
                name,
                self.lat.unwrap(),
                self.lon.unwrap()
            ))
        } else if let Some(name) = &self.short_name {
            f.write_fmt(format_args!(
                "{} ({:.5}, {:.5})",
                name,
                self.lat.unwrap(),
                self.lon.unwrap()
            ))
        } else {
            f.write_fmt(format_args!(
                ":shrug: ({:.5}, {:.5})",
                self.lat.unwrap(),
                self.lon.unwrap()
            ))
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Route {
    pub(crate) id: i32,
    pub(crate) operator: i32,
    // pub(crate) subroutes: Vec<Subroute>,
    pub(crate) code: String,
    pub(crate) name: String,
}

impl PartialEq for Route {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Subroute {
    pub(crate) id: i32,
    pub(crate) flag: String,
    pub(crate) route: Rc<Route>,
}

impl Hash for Subroute {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state)
    }
}

impl PartialEq for Subroute {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for Subroute {}

impl Display for Subroute {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "{} ({};{})",
            self.route.code, self.flag, self.id
        ))
    }
}

pub struct SubrouteStops {
    pub subroute: i32,
    pub stops: Vec<i32>,
    pub diffs: Vec<Option<i32>>,
}
