use std::collections::HashMap;
use std::rc::Rc;

use crate::models::{Stop, Subroute};
use crate::pathfinder::SubrouteSuccessor;
use crate::{Route, World};

pub(crate) fn unidirectional_line_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 5,
            ..Stop::default()
        }),
    ];
    let subroutes = vec![Rc::new(Subroute {
        id: 1,
        flag: "Foo".to_string(),
        route: Rc::new(Route::default()),
    })];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map.insert(
                2,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 3,
                    idx: 1,
                }],
            );
            map.insert(
                3,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 4,
                    idx: 2,
                }],
            );
            map.insert(
                4,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 5,
                    idx: 3,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 4), (1.0, 1.0));
            map.insert((4, 5), (1.0, 1.0));
            map
        },
    };
    world
}

// Two subroutes in a line world
// Subroute 1:  1 - 2 - 3 - 4 - 5
// Subroute 2:  1 - - - - - 4 - 5
pub(crate) fn unidirectional_dual_subroute_early_skip_line_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 5,
            ..Stop::default()
        }),
    ];
    let subroutes = vec![
        Rc::new(Subroute {
            id: 1,
            flag: "Foo".to_string(),
            route: Rc::new(Route::default()),
        }),
        Rc::new(Subroute {
            id: 2,
            flag: "Foo but faster".to_string(),
            route: Rc::new(Route::default()),
        }),
    ];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map.insert(subroutes[1].id, subroutes[1].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map.insert(
                2,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 3,
                    idx: 1,
                }],
            );
            map.insert(
                3,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 4,
                    idx: 2,
                }],
            );
            map.insert(
                4,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 5,
                    idx: 3,
                }],
            );

            map.insert(
                1,
                vec![SubrouteSuccessor {
                    subroute: subroutes[1].clone(),
                    stop: 4,
                    idx: 0,
                }],
            );
            map.insert(
                4,
                vec![SubrouteSuccessor {
                    subroute: subroutes[1].clone(),
                    stop: 5,
                    idx: 1,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 4), (1.0, 1.0));
            map.insert((4, 5), (1.0, 1.0));
            map.insert((1, 4), (2.0, 2.0));
            map
        },
    };
    world
}

// Two subroutes in a line world
// Subroute 1:  1 - 2 - 3 - 4 - 5
// Subroute 2:  1 - 2 - - - - - 5
pub(crate) fn unidirectional_dual_subroute_late_skip_line_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 5,
            ..Stop::default()
        }),
    ];
    let subroutes = vec![
        Rc::new(Subroute {
            id: 1,
            flag: "Foo".to_string(),
            route: Rc::new(Route::default()),
        }),
        Rc::new(Subroute {
            id: 2,
            flag: "Foo but faster".to_string(),
            route: Rc::new(Route::default()),
        }),
    ];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map.insert(subroutes[1].id, subroutes[1].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 2,
                        idx: 0,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 2,
                        idx: 0,
                    },
                ],
            );
            map.insert(
                2,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 3,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 5,
                        idx: 1,
                    },
                ],
            );
            map.insert(
                3,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 4,
                    idx: 2,
                }],
            );
            map.insert(
                4,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 5,
                    idx: 3,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 4), (1.0, 1.0));
            map.insert((4, 5), (1.0, 1.0));
            map.insert((2, 5), (2.0, 2.0));
            map
        },
    };
    world
}

// Two routes in a line world
// Route 1:  1 - 2 - 3 - 4 - 5
// Route 2:  1 - - - - - 4 - 5
pub(crate) fn unidirectional_dual_route_early_skip_line_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 5,
            ..Stop::default()
        }),
    ];

    let route = Rc::new(Route::default());
    let subroutes = vec![
        Rc::new(Subroute {
            id: 1,
            flag: "Foo".to_string(),
            route: route.clone(),
        }),
        Rc::new(Subroute {
            id: 2,
            flag: "Foo but faster".to_string(),
            route,
        }),
    ];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map.insert(subroutes[1].id, subroutes[1].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 2,
                        idx: 0,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 4,
                        idx: 0,
                    },
                ],
            );
            map.insert(
                2,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 3,
                    idx: 1,
                }],
            );
            map.insert(
                3,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 4,
                    idx: 2,
                }],
            );
            map.insert(
                4,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 5,
                        idx: 3,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 5,
                        idx: 1,
                    },
                ],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 4), (1.0, 1.0));
            map.insert((4, 5), (1.0, 1.0));
            map.insert((1, 4), (2.0, 2.0));
            map
        },
    };
    world
}

// Two routes in a line world
// Route 1:  1 - 2 - 3 - 4 - 5
// Route 2:  1 - 2 - - - - - 5
pub(crate) fn unidirectional_dual_route_late_skip_line_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 5,
            ..Stop::default()
        }),
    ];
    let route = Rc::new(Route::default());
    let subroutes = vec![
        Rc::new(Subroute {
            id: 1,
            flag: "Foo".to_string(),
            route: route.clone(),
        }),
        Rc::new(Subroute {
            id: 2,
            flag: "Foo but faster".to_string(),
            route,
        }),
    ];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map.insert(subroutes[1].id, subroutes[1].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 2,
                        idx: 0,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 2,
                        idx: 0,
                    },
                ],
            );
            map.insert(
                2,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 3,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 5,
                        idx: 1,
                    },
                ],
            );
            map.insert(
                3,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 4,
                    idx: 2,
                }],
            );
            map.insert(
                4,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 5,
                    idx: 3,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 4), (1.0, 1.0));
            map.insert((4, 5), (1.0, 1.0));
            map.insert((2, 5), (2.0, 2.0));
            map
        },
    };
    world
}

// One subroute goes back and forth
pub(crate) fn single_route_bidirectional_line_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 5,
            ..Stop::default()
        }),
    ];
    let subroutes = vec![Rc::new(Subroute {
        id: 1,
        flag: "Foo".to_string(),
        route: Rc::new(Route::default()),
    })];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map.insert(
                2,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 3,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 1,
                        idx: 7,
                    },
                ],
            );
            map.insert(
                3,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 4,
                        idx: 2,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 2,
                        idx: 6,
                    },
                ],
            );
            map.insert(
                4,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 5,
                        idx: 3,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 3,
                        idx: 5,
                    },
                ],
            );
            map.insert(
                5,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 4,
                    idx: 4,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 1), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 2), (1.0, 1.0));
            map.insert((3, 4), (1.0, 1.0));
            map.insert((4, 3), (1.0, 1.0));
            map.insert((4, 5), (1.0, 1.0));
            map.insert((5, 4), (1.0, 1.0));
            map
        },
    };
    world
}
// One subroute goes back and forth
pub(crate) fn dual_subroute_bidirectional_line_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 5,
            ..Stop::default()
        }),
    ];
    let route = Rc::new(Route::default());
    let subroutes = vec![
        Rc::new(Subroute {
            id: 1,
            flag: "Foo".to_string(),
            route: route.clone(),
        }),
        Rc::new(Subroute {
            id: 2,
            flag: "Foo in reverse".to_string(),
            route,
        }),
    ];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map.insert(subroutes[1].id, subroutes[1].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map.insert(
                2,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 3,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 1,
                        idx: 7,
                    },
                ],
            );
            map.insert(
                3,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 4,
                        idx: 2,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 2,
                        idx: 6,
                    },
                ],
            );
            map.insert(
                4,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 5,
                        idx: 3,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 3,
                        idx: 5,
                    },
                ],
            );
            map.insert(
                5,
                vec![SubrouteSuccessor {
                    subroute: subroutes[1].clone(),
                    stop: 4,
                    idx: 4,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 1), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 2), (1.0, 1.0));
            map.insert((3, 4), (1.0, 1.0));
            map.insert((4, 3), (1.0, 1.0));
            map.insert((4, 5), (1.0, 1.0));
            map.insert((5, 4), (1.0, 1.0));
            map
        },
    };
    world
}

pub(crate) fn unidirectional_loop_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 5,
            ..Stop::default()
        }),
    ];
    let subroutes = vec![Rc::new(Subroute {
        id: 1,
        flag: "Foo".to_string(),
        route: Rc::new(Route::default()),
    })];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map.insert(
                2,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 3,
                    idx: 1,
                }],
            );
            map.insert(
                3,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 4,
                    idx: 2,
                }],
            );
            map.insert(
                4,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 5,
                    idx: 3,
                }],
            );
            map.insert(
                5,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 1,
                    idx: 4,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 4), (1.0, 1.0));
            map.insert((4, 5), (1.0, 1.0));
            map.insert((5, 1), (1.0, 1.0));
            map
        },
    };
    world
}

pub(crate) fn bidirectional_loop_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 5,
            ..Stop::default()
        }),
    ];
    let route = Rc::new(Route::default());
    let subroutes = vec![
        Rc::new(Subroute {
            id: 1,
            flag: "Foo".to_string(),
            route: route.clone(),
        }),
        Rc::new(Subroute {
            id: 2,
            flag: "Foo in reverse".to_string(),
            route,
        }),
    ];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map.insert(subroutes[1].id, subroutes[1].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 2,
                        idx: 0,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 5,
                        idx: 4,
                    },
                ],
            );
            map.insert(
                2,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 3,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 1,
                        idx: 3,
                    },
                ],
            );
            map.insert(
                3,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 4,
                        idx: 2,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 2,
                        idx: 2,
                    },
                ],
            );
            map.insert(
                4,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 5,
                        idx: 3,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 3,
                        idx: 1,
                    },
                ],
            );
            map.insert(
                5,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 1,
                        idx: 4,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 4,
                        idx: 0,
                    },
                ],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 1), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 2), (1.0, 1.0));
            map.insert((3, 4), (1.0, 1.0));
            map.insert((4, 3), (1.0, 1.0));
            map.insert((4, 5), (1.0, 1.0));
            map.insert((5, 4), (1.0, 1.0));
            map.insert((5, 1), (1.0, 1.0));
            map.insert((1, 5), (1.0, 1.0));
            map
        },
    };
    world
}

// Route goes along the outside of the Y, in loop
// Order: 1 - 2 - 3 - 2 - 4 - 2 - 1
//
//        /- 3
//  1 -- 2
//         \- 4
pub(crate) fn single_route_single_direction_loop_y_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
    ];
    let subroutes = vec![Rc::new(Subroute {
        id: 1,
        flag: "Foo".to_string(),
        route: Rc::new(Route::default()),
    })];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map.insert(
                2,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 3,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 4,
                        idx: 3,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 1,
                        idx: 5,
                    },
                ],
            );
            map.insert(
                3,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 2,
                }],
            );
            map.insert(
                4,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 4,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 1), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 2), (1.0, 1.0));
            map.insert((2, 4), (1.0, 1.0));
            map.insert((4, 2), (1.0, 1.0));
            map.insert((4, 1), (1.0, 1.0));
            map.insert((1, 4), (1.0, 1.0));
            map
        },
    };
    world
}

// Route #1 goes 1-2-3 and route #2 goes 1-2-4
//         /- 3
//  1 -- 2
//         \- 4
pub(crate) fn dual_route_unidirectional_subroute_y_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
    ];
    let subroutes = vec![
        Rc::new(Subroute {
            id: 1,
            flag: "Foo".to_string(),
            route: Rc::new(Route::default()),
        }),
        Rc::new(Subroute {
            id: 2,
            flag: "Bar".to_string(),
            route: Rc::new(Route::default()),
        }),
    ];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map.insert(subroutes[1].id, subroutes[1].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map.insert(
                2,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 3,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 4,
                        idx: 1,
                    },
                ],
            );
            map.insert(
                3,
                vec![SubrouteSuccessor {
                    subroute: subroutes[1].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 1), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 2), (1.0, 1.0));
            map.insert((2, 4), (1.0, 1.0));
            map.insert((4, 2), (1.0, 1.0));
            map
        },
    };
    world
}

// Route #1 goes 1-2-3 (and back) and route #2 goes 1-2-4 (and back)
//         /- 3
//  1 -- 2
//         \- 4
pub(crate) fn dual_route_bidirectional_subroute_y_world() -> World {
    let stops = vec![
        Rc::new(Stop {
            id: 1,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 2,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 3,
            ..Stop::default()
        }),
        Rc::new(Stop {
            id: 4,
            ..Stop::default()
        }),
    ];
    let route1 = Rc::new(Route::default());
    let route2 = Rc::new(Route::default());
    let subroutes = vec![
        Rc::new(Subroute {
            id: 1,
            flag: "Foo".to_string(),
            route: route1.clone(),
        }),
        Rc::new(Subroute {
            id: 2,
            flag: "Foo in reverse".to_string(),
            route: route1,
        }),
        Rc::new(Subroute {
            id: 3,
            flag: "Bar".to_string(),
            route: route2.clone(),
        }),
        Rc::new(Subroute {
            id: 4,
            flag: "Bar in reverse".to_string(),
            route: route2,
        }),
    ];

    let world = World {
        nodes: {
            let mut map = HashMap::new();
            map.insert(stops[0].id, stops[0].clone());
            map.insert(stops[1].id, stops[1].clone());
            map.insert(stops[2].id, stops[2].clone());
            map.insert(stops[3].id, stops[3].clone());
            map.insert(stops[4].id, stops[4].clone());
            map
        },
        clusters: HashMap::new(),
        subroutes: {
            let mut map = HashMap::new();
            map.insert(subroutes[0].id, subroutes[0].clone());
            map.insert(subroutes[1].id, subroutes[1].clone());
            map.insert(subroutes[2].id, subroutes[2].clone());
            map.insert(subroutes[3].id, subroutes[3].clone());
            map
        },
        node_succs: {
            let mut map = HashMap::new();
            map.insert(
                1,
                vec![SubrouteSuccessor {
                    subroute: subroutes[0].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map.insert(
                2,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[0].clone(),
                        stop: 3,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 1,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[2].clone(),
                        stop: 4,
                        idx: 1,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[3].clone(),
                        stop: 3,
                        idx: 1,
                    },
                ],
            );
            map.insert(
                3,
                vec![
                    SubrouteSuccessor {
                        subroute: subroutes[1].clone(),
                        stop: 2,
                        idx: 0,
                    },
                    SubrouteSuccessor {
                        subroute: subroutes[2].clone(),
                        stop: 2,
                        idx: 0,
                    },
                ],
            );
            map.insert(
                4,
                vec![SubrouteSuccessor {
                    subroute: subroutes[3].clone(),
                    stop: 2,
                    idx: 0,
                }],
            );
            map
        },
        dists: {
            let mut map = HashMap::new();
            map.insert((1, 2), (1.0, 1.0));
            map.insert((2, 1), (1.0, 1.0));
            map.insert((2, 3), (1.0, 1.0));
            map.insert((3, 2), (1.0, 1.0));
            map.insert((2, 4), (1.0, 1.0));
            map.insert((4, 2), (1.0, 1.0));
            map
        },
    };
    world
}
