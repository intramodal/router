use std::collections::HashMap;
use std::rc::Rc;

use crate::pathfinder::{Cost, Mode, NodeContext, NodeId, Pathfinder};
use crate::tests::worlds;

fn assert_candidate(
    candidate: (&Rc<NodeContext>, &Cost),
    id: NodeId,
    time_spent: u64,
    hops: u32,
    mode: Mode,
) {
    let (next_candidate_node_context, next_candidate_node_cost) = candidate;
    assert_eq!(next_candidate_node_context.mode, mode);
    assert_eq!(next_candidate_node_context.node.id, id);
    assert_eq!(next_candidate_node_cost.spent_time, time_spent);
    assert_eq!(next_candidate_node_cost.hops, hops);
}

fn assert_seen(seen: &HashMap<NodeId, Cost>, expected: &[(NodeId, Cost)]) {
    expected.iter().for_each(|(id, cost)| {
        assert_eq!(seen.get(id), Some(cost));
    });
}

#[test]
fn test_finite_frontier() {
    let mut pathfinder = Pathfinder::between_stops(worlds::unidirectional_line_world(), 1, 5, None);
    let subroute = pathfinder.graph.subroutes.get(&1).unwrap().clone();

    assert_eq!(pathfinder.frontier.len(), 1);
    assert_candidate(pathfinder.frontier.peek_min().unwrap(), 1, 0, 0, Mode::Foot);
    assert_seen(
        &pathfinder.seen,
        &[(
            1,
            Cost {
                hops: 0,
                spent_time: 0,
                remaining_time: u64::MAX / 2,
            },
        )],
    );
    assert_eq!(pathfinder.seen.len(), 1);

    pathfinder.step();
    assert_eq!(pathfinder.frontier.len(), 1);
    assert_candidate(
        pathfinder.frontier.peek_min().unwrap(),
        2,
        1,
        1,
        Mode::Subroute(subroute.clone()),
    );
    assert_seen(
        &pathfinder.seen,
        &[
            (1, Cost::new(0, 0, u64::MAX / 2)),
            (2, Cost::new(1, 1, u64::MAX / 2 - 1)),
        ],
    );

    pathfinder.step();
    pathfinder.step();
    pathfinder.step();
    assert_seen(
        &pathfinder.seen,
        &[
            (1, Cost::new(0, 0, u64::MAX / 2)),
            (2, Cost::new(1, 1, u64::MAX / 2 - 1)),
            (3, Cost::new(1, 2, u64::MAX / 2 - 2)),
            (4, Cost::new(1, 3, u64::MAX / 2 - 3)),
            (5, Cost::new(1, 4, u64::MAX / 2 - 4)),
        ],
    );
    let (best_context, best_cost) = pathfinder.best.as_ref().unwrap();
    assert_eq!(best_context.node.id, 4);
    assert_eq!(best_cost.spent_time, 4);
    assert_eq!(best_cost.hops, 1);

    // Consume the destination itself
    pathfinder.step();
    let (best_context, best_cost) = pathfinder.best.unwrap();
    assert_eq!(best_context.node.id, 4);
    assert_eq!(best_cost.spent_time, 4);
    assert_eq!(best_cost.hops, 1);
    assert!(pathfinder.frontier.is_empty());
}
