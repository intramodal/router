/*
    Intermodal's router
    Copyright (C) 2022  Cláudio Pereira

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use geo::Point;
use serde::Deserialize;

static SERVER: &str = "http://localhost:5000";
// static ENDPOINT: &str = "{}/route/v1/driving/{}?alternatives=false&steps=false&hints=;";

pub async fn get_distance_between_points(
    a: &Point,
    b: &Point,
) -> Result<(f64, f64), Box<dyn std::error::Error>> {
    let coord_string = format!("{},{};{},{}", a.x(), a.y(), b.x(), b.y());

    // println!("{}/route/v1/driving/{}?alternatives=false&steps=false&hints=;", SERVER, coord_string);
    // let resp = reqwest::get(&format!("{}/route/v1/driving/{}?alternatives=false&steps=false", SERVER, coord_string))
    //     .await?
    //     .text()
    //     .await?;
    // println!("{}", resp);

    let resp = reqwest::get(&format!("{}/route/v1/driving/{}?alternatives=false&steps=false", SERVER, coord_string))
        .await?
        .json::<RoutingResponse>()
        .await?;
    Ok((resp.routes[0].distance, resp.routes[0].duration))
}

#[derive(Deserialize)]
struct RoutingResponse {
    routes: Vec<Route>,
    waypoints: Vec<Waypoint>,
}
#[derive(Deserialize)]
struct Route {
    geometry: String,
    legs: Vec<RouteLeg>,
    weight: f64,
    duration: f64,
    distance: f64,
    weight_name: String,
}

#[derive(Deserialize)]
struct RouteLeg {
    steps: Vec<String>,
    summary: String,
    weight: f64,
    duration: f64,
    distance: f64,
}

#[derive(Deserialize)]
struct Waypoint {
    hint: String,
    distance: f64,
    name: String,
    location: [f64; 2],
}
