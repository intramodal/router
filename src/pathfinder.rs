/*
    Intermodal's router
    Copyright (C) 2022  Cláudio Pereira

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::models::{Stop, Subroute};
use crate::nearest_stops;
use geo_types::Point;
use priority_queue::DoublePriorityQueue;
use std::cmp::Ordering;
use std::collections::{BTreeSet, HashMap};
use std::fmt::{Debug, Display, Formatter};
use std::hash::Hash;
use std::rc::Rc;

const WALK_SPEED: u64 = 5000 / 60; // Meters / minute

pub type NodeId = i32;
pub type RouteId = i32;
pub type SubrouteId = i32;

#[derive(Clone)]
pub struct SubrouteSuccessor {
    pub subroute: Rc<Subroute>,
    pub stop: NodeId,
    pub idx: usize,
}

pub type NodeRouteSuccessors = HashMap<NodeId, Vec<SubrouteSuccessor>>;
pub type Nodes = HashMap<NodeId, Rc<Stop>>;
pub type Subroutes = HashMap<SubrouteId, Rc<Subroute>>;

#[derive(Clone)]
pub struct World {
    pub nodes: Nodes,
    pub clusters: HashMap<NodeId, BTreeSet<NodeId>>,
    pub subroutes: Subroutes,
    pub node_succs: NodeRouteSuccessors,
    pub dists: HashMap<(NodeId, NodeId), (f64, f64)>,
}

#[derive(Eq, PartialEq, Clone, Debug)]
pub struct Cost {
    // Times a vehicle hop happened
    pub hops: u32,
    // Time spent until the current node
    pub spent_time: u64,
    // Prediction on how long it takes to reach the destination
    pub remaining_time: u64,
}

impl Cost {
    pub(crate) fn new(hops: u32, spent_time: u64, remaining_time: u64) -> Self {
        Self {
            hops,
            spent_time,
            remaining_time,
        }
    }
}

#[derive(Eq, PartialEq, Clone, Hash)]
pub enum Mode {
    Foot,
    Subroute(Rc<Subroute>),
}

impl Debug for Mode {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            Mode::Foot => f.write_str("foot"),
            Mode::Subroute(subroute) => f.write_fmt(format_args!("{}", subroute)),
        }
    }
}

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
pub struct NodeContext {
    pub node: Rc<Stop>,
    pub mode: Mode,
    pub prev: Option<Rc<NodeContext>>,
}

impl Display for NodeContext {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{} {:?}", self.node, self.mode))
    }
}

impl PartialOrd for Cost {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Cost {
    fn cmp(&self, other: &Self) -> Ordering {
        let hops_cmp = self.hops.cmp(&other.hops);
        match hops_cmp {
            Ordering::Equal => (self.spent_time + self.remaining_time)
                .cmp(&(other.spent_time + other.remaining_time)),
            _ => hops_cmp,
        }
    }
}

pub struct Pathfinder {
    pub(crate) frontier: DoublePriorityQueue<Rc<NodeContext>, Cost>,
    pub(crate) seen: HashMap<NodeId, Cost>,
    // End node + Remaining distance
    dst: HashMap<NodeId, u64>,
    pub(crate) graph: Rc<World>,
    pub(crate) best: Option<(Rc<NodeContext>, Cost)>,
}

impl Pathfinder {
    pub fn between_points(
        graph: World,
        from: Point,
        to: Point,
        predicted_duration: Option<u64>,
    ) -> Self {
        // TODO this is a bad approach.
        // It is meant to be a functional proof of concept
        // But should be replaced later
        let origin_nearest = nearest_stops(&from, graph.nodes.values(), 10);
        let dst_nearest = nearest_stops(&to, graph.nodes.values(), 10);

        if origin_nearest.is_empty() {
            panic!();
        }
        if dst_nearest.is_empty() {
            panic!();
        }

        let initial_cost = Cost::new(0, 0, predicted_duration.unwrap_or(u64::MAX / 2));
        let mut frontier = DoublePriorityQueue::<Rc<NodeContext>, Cost>::new();
        let mut seen = HashMap::new();

        for (initial_node, dist) in origin_nearest {
            if let Some(node_cluster) = graph.clusters.get(&initial_node.id) {
                for from_cluster_node in node_cluster {
                    seen.insert(*from_cluster_node, initial_cost.clone());
                    frontier.push(
                        Rc::new(NodeContext {
                            node: graph.nodes.get(&from_cluster_node).unwrap().clone(),
                            mode: Mode::Foot,
                            prev: None,
                        }),
                        Cost::new(
                            if dist > 1000 { 1 } else { 0 },
                            dist / (5000 / 60),
                            initial_cost.remaining_time,
                        ),
                    );
                }
            } else {
                seen.insert(initial_node.id, initial_cost.clone());
                frontier.push(
                    Rc::new(NodeContext {
                        node: initial_node,
                        mode: Mode::Foot,
                        prev: None,
                    }),
                    Cost::new(
                        if dist > 1000 { 1 } else { 0 },
                        dist / (5000 / 60),
                        initial_cost.remaining_time,
                    ),
                );
            }
        }

        let state = Self {
            frontier,
            seen,
            dst: dst_nearest
                .into_iter()
                .map(|(node, dist)| (node.id, dist))
                .collect::<HashMap<NodeId, u64>>(),
            graph: Rc::new(graph),
            best: None,
        };
        state
    }

    pub fn between_stops(
        graph: World,
        from: NodeId,
        to: NodeId,
        predicted_duration: Option<u64>,
    ) -> Self {
        let initial_cost = Cost::new(0, 0, predicted_duration.unwrap_or(u64::MAX / 2));
        let mut frontier = DoublePriorityQueue::<Rc<NodeContext>, Cost>::new();
        let mut seen = HashMap::new();

        if let Some(node_cluster) = graph.clusters.get(&from) {
            for from_cluster_node in node_cluster {
                seen.insert(*from_cluster_node, initial_cost.clone());
                frontier.push(
                    Rc::new(NodeContext {
                        node: graph.nodes.get(&from_cluster_node).unwrap().clone(),
                        mode: Mode::Foot,
                        prev: None,
                    }),
                    Cost::new(0, 0, initial_cost.remaining_time),
                );
            }
        } else {
            seen.insert(from, initial_cost.clone());
            frontier.push(
                Rc::new(NodeContext {
                    node: graph.nodes.get(&from).unwrap().clone(),
                    mode: Mode::Foot,
                    prev: None,
                }),
                Cost::new(0, 0, initial_cost.remaining_time),
            );
        }

        let state = Self {
            frontier,
            seen,
            dst: [to]
                .into_iter()
                .map(|node| (node, 0))
                .collect::<HashMap<NodeId, u64>>(),
            graph: Rc::new(graph),
            best: None,
        };
        state
    }

    pub fn step(&mut self) {
        if let Some((curr_ctx, curr_cost)) = self.frontier.pop_min() {
            let succs = self.graph.node_succs.get(&curr_ctx.node.id).clone();

            if succs.is_none() {
                return;
            }

            for subroute_succ in succs.unwrap().clone() {
                let mut hops = curr_cost.hops;
                if curr_ctx.mode != Mode::Subroute(subroute_succ.subroute.clone()) {
                    hops += 1;
                }
                let predicted_duration =
                    self.predicted_duration(curr_ctx.node.id, subroute_succ.stop);
                let successor_cost = Pathfinder::succ_cost(hops, &curr_cost, predicted_duration);

                if !self.is_succ_cheaper(subroute_succ.stop, &successor_cost) {
                    continue;
                }

                self.check_destination_hit(subroute_succ.stop, &curr_ctx, hops, &successor_cost);
                self.update_frontier_and_seen(&curr_ctx, &subroute_succ, successor_cost);
            }
        }
    }

    #[inline]
    fn check_destination_hit(
        &mut self,
        successor_node: NodeId,
        curr_ctx: &Rc<NodeContext>,
        hops: u32,
        successor_cost: &Cost,
    ) {
        if let Some(remaining_dist) = self.dst.get(&successor_node) {
            let hops = hops + if *remaining_dist > 1000 { 1 } else { 0 };
            if let Some((_prev_best_node, prev_best_cost)) = &self.best {
                let total_time_spent = successor_cost.spent_time + remaining_dist / WALK_SPEED;
                if total_time_spent < prev_best_cost.spent_time {
                    self.best = Some((
                        curr_ctx.clone(),
                        // FIXME wrong prediction
                        Cost::new(hops, total_time_spent, 0),
                    ));
                }
            } else {
                self.best = Some((
                    curr_ctx.clone(),
                    // FIXME wrong prediction
                    Cost::new(
                        hops,
                        successor_cost.spent_time + remaining_dist / WALK_SPEED,
                        0,
                    ),
                ));
            }
        }
    }

    #[inline]
    fn update_frontier_and_seen(
        &mut self,
        node: &Rc<NodeContext>,
        subroute_successor: &SubrouteSuccessor,
        cost: Cost,
    ) {
        if let Some(successor_cluster) = self.graph.clusters.get(&subroute_successor.stop) {
            for cluster_node in successor_cluster {
                self.frontier.push_decrease(
                    Rc::new(NodeContext {
                        node: self.graph.nodes.get(cluster_node).unwrap().clone(),
                        mode: Mode::Subroute(subroute_successor.subroute.clone()),
                        prev: Some(node.clone()),
                    }),
                    cost.clone(),
                );

                self.seen.insert(*cluster_node, cost.clone());
            }
        } else {
            self.frontier.push_decrease(
                Rc::new(NodeContext {
                    node: self
                        .graph
                        .nodes
                        .get(&subroute_successor.stop)
                        .unwrap()
                        .clone(),
                    mode: Mode::Subroute(subroute_successor.subroute.clone()),
                    prev: Some(node.clone()),
                }),
                cost.clone(),
            );

            self.seen.insert(subroute_successor.stop, cost.clone());
        }
    }

    #[inline]
    fn succ_cost(hops: u32, curr_cost: &Cost, predicted_duration: f64) -> Cost {
        let predicted_duration = predicted_duration as u64;
        Cost::new(
            hops,
            curr_cost.spent_time + predicted_duration,
            curr_cost.remaining_time.saturating_sub(predicted_duration),
        )
    }

    #[inline]
    fn is_succ_cheaper(&self, curr_id: NodeId, successor_cost: &Cost) -> bool {
        if let Some(previous_cost) = self.seen.get(&curr_id) {
            if previous_cost.spent_time < successor_cost.spent_time
                || (previous_cost.spent_time == successor_cost.spent_time
                    && previous_cost.hops < successor_cost.hops)
            {
                return false;
            }
        }
        true
    }

    #[inline]
    fn predicted_duration(&self, from: NodeId, to: NodeId) -> f64 {
        self.graph.dists.get(&(from, to)).unwrap_or(&(1.0, 1.0)).1
    }
}

#[cfg(test)]
mod tests {
    use std::cmp::Ordering;

    use priority_queue::DoublePriorityQueue;

    use crate::pathfinder::Cost;

    #[test]
    fn test_cost_order_equal_remaining() {
        let a = Cost::new(0, 0, 10);
        let b = Cost::new(0, 0, 10);
        assert_eq!(a.cmp(&b), Ordering::Equal);
    }

    #[test]
    fn test_cost_order_bigger_remaining() {
        let a = Cost::new(0, 0, 10);
        let b = Cost::new(0, 0, 0);
        assert_eq!(a.cmp(&b), Ordering::Greater);
    }

    #[test]
    fn test_cost_order_smaller_remaining() {
        let a = Cost::new(0, 0, 0);
        let b = Cost::new(0, 0, 10);
        assert_eq!(a.cmp(&b), Ordering::Less);
    }

    #[test]
    fn test_cost_order_equal_total_time_1() {
        let a = Cost::new(0, 0, 10);
        let b = Cost::new(0, 0, 10);
        assert_eq!(a.cmp(&b), Ordering::Equal);
    }

    #[test]
    fn test_cost_order_equal_total_time_2() {
        let a = Cost::new(0, 5, 5);
        let b = Cost::new(0, 5, 5);
        assert_eq!(a.cmp(&b), Ordering::Equal);
    }

    #[test]
    fn test_cost_order_greater_total_time() {
        let a = Cost::new(0, 10, 5);
        let b = Cost::new(0, 0, 10);
        assert_eq!(a.cmp(&b), Ordering::Greater);
    }

    #[test]
    fn test_cost_order_lesser_total_time() {
        let a = Cost::new(0, 10, 0);
        let b = Cost::new(0, 5, 10);
        assert_eq!(a.cmp(&b), Ordering::Less);
    }

    #[test]
    fn test_cost_order_lesser_hops_time() {
        let a = Cost::new(0, 100, 100);
        let b = Cost::new(5, 10, 10);
        assert_eq!(a.cmp(&b), Ordering::Less);
    }

    #[test]
    fn test_cost_order_greater_hops_time() {
        let a = Cost::new(5, 10, 0);
        let b = Cost::new(0, 100, 100);
        assert_eq!(a.cmp(&b), Ordering::Greater);
    }

    #[test]
    fn test_cost_priority_min() {
        let mut queue = DoublePriorityQueue::new();
        queue.push("lowest", Cost::new(0, 0, 0));
        queue.push("highest", Cost::new(100, 0, 0));
        assert_eq!(queue.peek_min(), Some((&"lowest", &Cost::new(0, 0, 0))));
    }

    #[test]
    fn test_cost_priority_max() {
        let mut queue = DoublePriorityQueue::new();
        queue.push("lowest", Cost::new(0, 0, 0));
        queue.push("highest", Cost::new(100, 0, 0));

        assert_eq!(queue.peek_max(), Some((&"highest", &Cost::new(100, 0, 0))));
    }

    #[test]
    fn test_cost_priority_change() {
        let mut queue = DoublePriorityQueue::new();
        queue.push("lowest", Cost::new(10, 0, 0));
        queue.push("highest", Cost::new(100, 0, 0));

        queue.push_decrease("lowest", Cost::new(0, 0, 0));

        assert_eq!(queue.peek_min(), Some((&"lowest", &Cost::new(0, 0, 0))));
    }
}
