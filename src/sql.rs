/*
    Intermodal's router
    Copyright (C) 2022  Cláudio Pereira

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use sqlx::PgPool;
use std::collections::HashMap;
use std::rc::Rc;

use crate::models::{Stop, Subroute, SubrouteStops};
use crate::{Route, RouteId, SubrouteId};

type Result<T> = std::result::Result<T, ()>;

pub(crate) async fn fetch_stops(pool: &PgPool) -> Result<Vec<Stop>> {
    let stops: Vec<Stop> = sqlx::query_as!(
        Stop,
        r#"SELECT *
        FROM Stops
        WHERE id IN (
            SELECT DISTINCT subroute_stops.stop
            FROM subroute_stops
            JOIN subroutes on subroute_stops.subroute = subroutes.id
            JOIN routes on routes.id = subroutes.route
            WHERE routes.operator = 2
        )
        "#
    )
    .fetch_all(pool)
    .await
    .map_err(|_err| ())?;

    Ok(stops)
}

pub(crate) async fn fetch_subroutes(pool: &PgPool) -> Result<HashMap<SubrouteId, Subroute>> {
    let res = sqlx::query!(
        r#"
SELECT routes.id as route,
    routes.code as code,
    routes.name as name,
    routes.operator as operator,
    routes.type as service_type,
    routes.circular as circular,
    routes.main_subroute as main_subroute,
    routes.active as active,
    route_types.badge_text_color as text_color,
    route_types.badge_bg_color as bg_color,
    subroutes.id as "subroute!: i32",
    subroutes.flag as "subroute_flag!: String"
FROM routes
JOIN route_types on routes.type = route_types.id
JOIN subroutes ON routes.id = subroutes.route
WHERE routes.operator=2
ORDER BY routes.id asc
"#
    )
    .fetch_all(pool)
    .await
    .map_err(|_err| ())?;

    let mut routes: HashMap<RouteId, Rc<Route>> = HashMap::new();
    let mut subroutes: HashMap<SubrouteId, Subroute> = HashMap::new();

    for row in res {
        let route = routes.entry(row.route).or_insert(Rc::new(Route {
            id: row.route,
            code: row.code.unwrap_or("-".to_string()),
            name: row.name,
            operator: row.operator,
        }));
        subroutes.insert(
            row.subroute,
            Subroute {
                id: row.subroute,
                flag: row.subroute_flag,
                route: route.clone(),
            },
        );
    }

    Ok(subroutes)
}

pub(crate) async fn fetch_all_subroute_stops(pool: &PgPool) -> Result<Vec<SubrouteStops>> {
    let res = sqlx::query!(
        r#"
SELECT
    subroutes.id,
	array_agg(subroute_stops.stop ORDER BY subroute_stops.idx) as "stops!: Vec<i32>"
FROM subroutes
JOIN subroute_stops ON subroutes.id = subroute_stops.subroute
JOIN routes on routes.id = subroutes.route
WHERE routes.operator = 2
GROUP BY subroutes.id
ORDER BY subroutes.id asc
    "#,
    )
    .fetch_all(pool)
    .await
    .map_err(|_err| ())?;

    let subroute_stops = res
        .into_iter()
        .map(|row| SubrouteStops {
            subroute: row.id,
            stops: row.stops,
            diffs: vec![],
        })
        .collect::<Vec<_>>();

    Ok(subroute_stops)
}
