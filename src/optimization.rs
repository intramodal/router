/*
    Intermodal's router
    Copyright (C) 2022  Cláudio Pereira

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::hash_map::{Entry, Values};
use std::collections::{BTreeSet, HashMap};
use std::ptr;
use std::rc::Rc;

use geo::{GeodesicDistance, Point};
use itertools::Itertools;
use rand::prelude::*;
use rayon::prelude::*;

use crate::models::Stop;
use crate::pathfinder::NodeId;

const CLUSTER_THRESHOLD: u64 = 120_000;

pub fn cluster_stops(stops: Vec<Rc<Stop>>) -> HashMap<NodeId, BTreeSet<NodeId>> {
    let mut rng = thread_rng();

    println!("Done fetching stops");
    let stop_coords: Vec<Point> = stops
        .iter()
        .map(|stop| {
            (
                stop.lon.unwrap_or_else(|| rng.gen_range(-180.0..180.0)),
                stop.lat.unwrap_or_else(|| rng.gen_range(-90.0..90.0)),
            )
                .into()
        })
        .collect_vec();

    let stop_count = stops.len();
    println!("Checking proximity for {} stops", stop_count);

    let dist_mat = stop_coords
        .par_iter()
        .enumerate()
        .map(|(i, s1)| {
            stop_coords.par_iter().enumerate().map(move |(j, s2)| {
                (
                    i,
                    j,
                    if s1.x() == 0.0 || s2.x() == 0.0 {
                        u64::MAX
                    } else {
                        (s1.geodesic_distance(s2) * 1000.0) as u64
                    },
                )
            })
        })
        .flatten()
        .collect::<Vec<_>>();

    type StopCluster = Rc<RefCell<BTreeSet<usize>>>;

    let mut stop_clusters: HashMap<usize, StopCluster> = HashMap::new();

    dist_mat
        .iter()
        .filter(|(i, j, dist)| i < j && *dist < CLUSTER_THRESHOLD)
        .sorted_by_key(|(_, _, dist)| dist)
        .for_each(|(s1, s2, _dist)| {
            let cluster = match stop_clusters.entry(*s1) {
                Entry::Occupied(e) => {
                    let cluster: Rc<RefCell<BTreeSet<usize>>> = e.get().clone();
                    {
                        let mut cluster = (&*cluster).borrow_mut();
                        cluster.insert(*s2);
                    }
                    cluster
                }
                Entry::Vacant(e) => {
                    let cluster = Rc::new(RefCell::new({
                        let mut set = BTreeSet::new();
                        set.insert(*s1);
                        set.insert(*s2);
                        set
                    }));
                    e.insert(cluster.clone());
                    cluster
                }
            };

            match stop_clusters.entry(*s2) {
                Entry::Occupied(mut e) => {
                    let other_cluster: &RefCell<_> = e.get().borrow();
                    let same = { ptr::eq(other_cluster, cluster.borrow()) };
                    if !same {
                        let other_cluster = other_cluster.borrow();
                        (&*cluster).borrow_mut().extend(other_cluster.iter());
                    }
                    e.insert(cluster);
                }
                Entry::Vacant(e) => {
                    e.insert(cluster);
                }
            };
        });

    // stop_clusters.borrow().values().for_each(|cluster| {
    //     let cluster = cluster.deref().borrow();
    //     cluster.iter().for_each(|s| print!("{}, ", stops[*s]));
    //     println!();
    // });

    stop_clusters
        .into_iter()
        .map(|(stop_index, cluster)| {
            let stop = stops.get(stop_index).unwrap().clone();
            let borrowed_cluster: &RefCell<_> = cluster.borrow();
            let id_cluster = borrowed_cluster
                .borrow()
                .iter()
                .map(|index| stops.get(*index).unwrap().id)
                .collect();
            (stop.id, id_cluster)
        })
        .collect::<HashMap<NodeId, BTreeSet<NodeId>>>()
}

pub(crate) fn nearest_stops(
    point: &Point,
    stops: Values<NodeId, Rc<Stop>>,
    n: usize,
) -> Vec<(Rc<Stop>, u64)> {
    let mut rng = thread_rng();

    stops
        .map(|stop| {
            (
                stop.clone(),
                point.geodesic_distance(&Point::from((
                    stop.lon.unwrap_or_else(|| rng.gen_range(-180.0..180.0)),
                    stop.lat.unwrap_or_else(|| rng.gen_range(-90.0..90.0)),
                ))) as u64,
            )
        })
        .sorted_by_key(|(_, distance)| *distance)
        .take(n)
        .collect::<Vec<_>>()
}
