/*
    Intermodal's router
    Copyright (C) 2022  Cláudio Pereira

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

mod cache;
mod models;
mod optimization;
mod osrm;
mod pathfinder;
mod sql;
#[cfg(test)]
mod tests;

use crate::cache::load_world;
use crate::models::{Route, Stop};
use crate::optimization::nearest_stops;
use crate::osrm::get_distance_between_points;
use crate::pathfinder::{Mode, NodeContext, Pathfinder, RouteId, SubrouteId, World};
use geo_types::point;
use std::io;
use std::rc::Rc;

#[tokio::main]
async fn main() {
    let graph = load_world().await;

    let from = point! {x: -9.01743, y: 38.59697 };
    let to = point! {x: -9.06691 , y: 38.65385};

    let mut pathfinder = Pathfinder::between_points(
        graph,
        from,
        to,
        Some(get_distance_between_points(&from, &to).await.unwrap().1 as u64 * 2),
    );
    loop {
        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let mut steps = 0;
        match guess.trim() {
            "s" => {
                // Step
                if let Some((node, cost)) = (&pathfinder.frontier).peek_min() {
                    println!("{}, {:?}", node, cost);
                } else {
                    println!("Nothing to be done");
                    continue;
                }
                pathfinder.step();
                steps += 1;
            }
            "brr" => {
                while !pathfinder.frontier.is_empty() {
                    pathfinder.step();
                    steps += 1;
                    if steps % 1_000 == 0 {
                        if let Some(best) = &pathfinder.best {
                            println!("Frontier still not empty. Current candidate is:");
                            println!("h: {}, c: {}", best.1.hops, best.1.spent_time);
                            print_result(&best.0);
                        } else {
                            println!("Frontier still not empty. No candidate yet");
                        }
                        break;
                    }
                }
            }
            "f" => {
                // Frontier
                // pathfinder.frontier;
                for (node, cost) in pathfinder.frontier.iter().take(100) {
                    println!("{}, {:?}", node, cost);
                }
                continue;
            }
            _ => {
                println!("Invalid command: {}", guess);
                continue;
            }
        }

        if let Some(best) = &pathfinder.best {
            println!("Best found");
            println!(
                "h: {}, c: {}, fs:{}",
                best.1.hops,
                best.1.spent_time,
                pathfinder.frontier.len()
            );
            print_result(&best.0);
        }
        if pathfinder.frontier.is_empty() {
            return;
        }
    }
    // let result = dijskstra(graph, 13676, 26).unwrap();
    // println!("h: {}, c: {}", result.1.hops, result.1.time_spent);
    // print_result(result.0, &routes, &subroutes, subroute_to_route);
}

fn print_result(result: &Rc<NodeContext>) {
    let mode = match &result.mode {
        Mode::Foot => "Walkin' my way downtown".to_string(),
        Mode::Subroute(subroute) => subroute.to_string(),
    };
    println!("{}, {}", result.node, mode);
    if let Some(pred) = &result.prev {
        print_result(pred);
    }
}
